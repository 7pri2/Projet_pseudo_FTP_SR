# Projet SR pseudo FTP

Projet de SR à l'université Grenoble Alpes: création d'un client et serveur avec
un comportement inspiré de FTP

> Cyprien Eymond Laritaz
> Florian Argaud

# Travail réalisé
## Commandes
Le serveur est à même de répondre aux requêtes envoyées par le client parmis 
lesquelles:
1. `get *fichier*`
2. `cd *chemin*`
3. `pwd`
4. `ls`
5. `bye`

## Fonctionnement
Les serveurs esclaves sont lancés en lançant le programme `server` avec un 
paramètre quelconque. Ils sont en écoute sur le port **2122**.


Le serveur démarre et attend **NB_SLAVES** adresses ip d'esclaves. Il garde 
juste leur adresse en mémoire.Le serveur accepte des clients sur le port 
**2121**, dès qu'il en accepte un, il fork et traite les requêtes du client 
jusqu'à ce qu'il demande la fin de la communication.


Le client démarre et pour se connecter au serveur, l'utilisateur doit fournir le
mot de passe codé en dur *motdepasse*.


Lorsqu'une commande est tapée par l'utilisateur dans le client, le client envoie
tout d'abord un octet de signalisation au serveur maître indiquant quelle 
commande effectuer (**GET**,**CD**,...). Le serveur maître, si la requête est un
**pwd** ou un **cd** effectue la commande en affichant sa variable interne `pwd`
ou en la modifiant.Dans le cas où la requête n'est pas l'une des précédentes, il
parcoure la liste d'esclaves pour en trouver un de disponible et lui transmet 
cet octet de signalisation. Il lui transmet aussi l'adresse ip du client ainsi 
que sa variable interne `pwd`.


Une fois ça fait, le client se met en écoute sur le port **2123** et accepte une 
connexion du serveur esclave qui récupère les informations nécessaires telles 
que le nom du fichier à transférer, le répertoire à afficher, et envoie au 
client les informations demandées si elles existent. Après la réalisation de la 
requête, la connexion entre le client et l'esclave est terminée et le client 
peut continuer ses intéractions avec le maître pour envoyer de nouvelles 
commandes.

### Transfert de fichier
Lorsqu'on souhaite télécharger un fichier, si le fichier existe déjà en partie 
dans le répertoire courant, ne seront téléchargés que les bytes restants pour 
compléter le fichier. Si le fichier sur le serveur est plus récent, le fichier 
entier et téléchargé.

## Tests
Les fonctionnalités ont été testées avec un serveur maître, un serveur esclave 
et un client tous trois sur la même machine.


Des tests concernant toutes les fonctions notifiées ci-avant ont été faits dans 
les conditions décrites précédemment et elles fonctionnent bien. Si on essaie de
faire un `cd ../..` ça ne fonctionne pas, pour correctement retourner en arrière
dans l'arborescence, il faut faire un unique `..`; la raison de ce bug est 
connue: lorsque le répertoire relatif soumis est **exactement** `..`, alors on 
retire tout le texte de la fin du `pwd` au premier \`/' que l'on rencontre, mais 
le cas n'est pas gèré pour une suite de `../..`.