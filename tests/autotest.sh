#!/bin/bash

if [ "$1" == "reset" ]
then
	rm -rf tests/client/remote* 
	rm -rf tests/server/local*
fi

cp client tests/client/
cp server tests/server/
cd tests/server/
./server > /dev/null 2> /dev/null & PID=$! 
echo $PID
echo "Remote files:"
#ls -lh remote* 2> /dev/null
ls -lh 2> /dev/null
cd ../client/
echo "Local files:"
#ls -lh local* 2> /dev/null
ls -lh 2> /dev/null
./client localhost
echo
echo -n Shuting down server...
kill -SIGINT $PID
echo ok.
