#ifndef __SERVER_H__
#define __SERVER_H__

#include "csapp.h"
#include "constants.h"

typedef char iplist[NB_SLAVES][MAX_NAME_LEN];

void slave_behaviour(int connectfd, char sig_byte, char* pwd);
void master_behaviour(int connectfd, char* client_hostname, iplist slaveIPs, char* pwd);
int transfer(rio_t rio, int connectfd, char* pwd);
int ls(int connectfd, char* pwd);

#endif /* end of include guard: __SERVER_H__ */
