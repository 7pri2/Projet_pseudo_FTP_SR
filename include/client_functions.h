#ifndef __CLIENT_FUNCTIONS_H__
#define __CLIENT_FUNCTIONS_H__

#include "csapp.h"

int get_slavefd();
void request_file(int connectfd, char* filename);
void request_ls(int clientfd);
void request_pwd(int clientfd);
void request_cd(int clientfd, char*);
char** str_split(char* base_str, int parts);

#endif /* end of include guard: __CLIENT_FUNCTIONS_H__ */
