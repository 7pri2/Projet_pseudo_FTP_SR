#ifndef __GENERIC_FUNCTIONS_H__
#define __GENERIC_FUNCTIONS_H__

void remove_cr(char* string);
void remove_last_dir(char* string);

#endif /* end of include guard: __GENERIC_FUNCTIONS_H__ */
