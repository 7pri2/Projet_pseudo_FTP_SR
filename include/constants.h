#ifndef __CONSTANTS_H__
#define __CONSTANTS_H__

#define UNUSED(x) (void)(x)
#define MAX_NAME_LEN 	256
#define BLOC_SIZE		256
#define MASTER_PORT		2121 
#define SLAVE_PORT		2122
#define DATA_PORT		2123
#define NB_SLAVES		1

/* Codes que le serveur peut envoyer aux clients */
#define OK		0
#define ERROR	1

/* Codes que les clients peuvent envoyer au serveur */
#define NO_CODE -1
#define GET	0
#define PUT	1
#define LS	2
#define PWD	3
#define CD	4

/* Codes pour rendre plus lisibles les forks */
#define CHILD		0
#define	FORK_ERR	-1

/* Colors */
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

#endif /* end of include guard: __CONSTANTS_H__ */
