#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include "csapp.h"
#include "constants.h"
#include "client_functions.h"
#include "generic_functions.h"

int main(int argc, char *argv[]) {
	/* clientfd : file descriptor du socket */
	int clientfd;
	char *host;	// Nom d'hote
	char buf[MAX_NAME_LEN], sig_byte;
	/* Tableau de strings pour stocker les differents mots de l'entree */
	char** split_input;
	rio_t rio_socket;

	/* Verification du nombre d'arguments, il doit y avoir uniquement le nom 
	 * d'hote ou l'ip */
	if (argc != 2) {
		fprintf(stderr,"Usage %s <host>\n", argv[0]);
		exit(0);
	}

	/* Recupération du nom d'hote du serveur */
	host = argv[1];

	/* Création du socket client et connexion au serveur */
	clientfd = Open_clientfd(host, MASTER_PORT);

	/* Initialisation de la lecture du socket */
	Rio_readinitb(&rio_socket, clientfd);

	printf("Connected to server\n");
	printf("Before you can sen requests, you need to authenticate yourself.\n");
	printf("password: ");
	Fgets(buf, MAX_NAME_LEN, stdin);
	remove_cr(buf);
	Rio_writen(clientfd, buf, MAX_NAME_LEN);
	Rio_readnb(&rio_socket, &sig_byte, 1);
	if(sig_byte == OK) {
		printf("Authenticated !\n");
		do {
			printf(ANSI_COLOR_CYAN "ftp > " ANSI_COLOR_RESET);

			/* On récupère le nom de fichier à récupérer sur le serveur via l'entrée standart */
			Fgets(buf, MAX_NAME_LEN, stdin);
			split_input = str_split(buf, 3); // On coupe en trois maximum
			remove_cr(split_input[0]);

			if(!strcmp(split_input[0], "get"))  {
				request_file(clientfd, split_input[1]);
			} else if(!strcmp(split_input[0], "put")) {
				; // WIP
			} else if(!strcmp(split_input[0], "ls")) {
				request_ls(clientfd);
			} else if(!strcmp(split_input[0], "rm")) {
				; // WIP
			} else if(!strcmp(split_input[0], "pwd")) {
				request_pwd(clientfd);
			} else if(!strcmp(split_input[0], "cd")) {
				request_cd(clientfd, split_input[1]);
			} else if(!strcmp(split_input[0], "mkdir")) {
				; // WIP
			} else {
				printf("Unrecognized command.\n");
			}
		} while(strcmp(split_input[0], "bye"));
	} else {
		printf("Wrong password !\n");
	}
	
	Close(clientfd); // On ferme le socket 
	exit(0);
}
