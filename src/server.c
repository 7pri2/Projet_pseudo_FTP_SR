#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <signal.h>
#include "constants.h"
#include "csapp.h"
#include "server_functions.h"

void father_handler() {
	setvbuf(stdout, NULL, _IONBF, 0);
	printf("Closing connexions...");
	kill(0, SIGINT);
	while(waitpid(-1, NULL, 0) >= 0);
	printf(" Ok\n");
	exit(0);
}

void child_handler() {
	/* Code a ajouter pour la gestion des clients */
	exit(0);
}

int main(int argc, char* argv[]) {
	UNUSED(argv);
	/* Si le serveur est lancé sans paramètre, il est le maitre, si il a un 
	 * paramètre, c'est l'esclave */
	if(argc == 2) {
	/* MODE ESCLAVE */
		int masterfd, clientfd, listenfd;
		rio_t rio_master;
		char sig_byte = NO_CODE;
		char pwd[MAX_NAME_LEN];

		/* Creation du socket pour la connexion au maitre */
		char client_ip_string[MAX_NAME_LEN];

		printf("Waiting for master to connect on port %d.\n", SLAVE_PORT);
		listenfd = Open_listenfd(SLAVE_PORT);
		while(1) {
			masterfd = Accept(listenfd, NULL, NULL);
			printf(ANSI_COLOR_CYAN "[CONNECTION]" ANSI_COLOR_RESET);
			printf("Master connection accepted, waiting for request.\n");

			/* On s'apprete a recevoir les infos du maitre */
			Rio_readinitb(&rio_master, masterfd);
			
			/* On recupere le code de requete */
			Rio_readnb(&rio_master, &sig_byte, 1);

			/* On recupere l'ip */
			Rio_readnb(&rio_master, &client_ip_string, MAX_NAME_LEN);
			
			/* On recupere le pwd */
			Rio_readnb(&rio_master, pwd, MAX_NAME_LEN);

			clientfd = Open_clientfd(client_ip_string, DATA_PORT);
			printf(ANSI_COLOR_CYAN "[CONNECTION]" ANSI_COLOR_RESET);
			printf("Client accepted slave connection.\n");

			slave_behaviour(clientfd, sig_byte, pwd);
			
			Close(clientfd);
			Close(masterfd);
		}
	} else {
	/* MODE MAITRE */
		struct sigaction sa_int, sa_chld;
		sa_chld.sa_handler = SIG_DFL;
		sa_chld.sa_flags = SA_NOCLDWAIT;
		sigaction(SIGCHLD, &sa_chld, NULL);
		sa_int.sa_handler = &father_handler;
		sigaction(SIGINT, &sa_int, NULL);

		/* clientfd : socket serveur
		 * listenfd : socket de communication
		 * slavefd	: sockets des esclaves */
		int clientfd, listenfd;
		iplist slaves_IPs;
		socklen_t clientlen;
		struct sockaddr_in clientaddr;	// Adresse des clients
		char *pwd = getenv("PWD");

		char client_ip_string[INET_ADDRSTRLEN];
		char client_hostname[MAX_NAME_LEN];

		clientlen = (socklen_t)sizeof(clientaddr);

		int i;
		for(i = 0; i < NB_SLAVES; ++i) {
			printf("Enter the IP address of slave (%d out of %d) : ",
					i+1, NB_SLAVES);
			scanf("%s", (char*)slaves_IPs[i]);
		}

		/* On crée le socket serveur, on le lie au port PORT et on écoute les 
		 * connexions entrantes */
		listenfd = Open_listenfd(MASTER_PORT);
		printf("Server listening on port %d\n", MASTER_PORT);

		while(1) {
			/* On accepte une connexion à un client */
			clientfd = Accept(listenfd, (SA*)&clientaddr, &clientlen);
			switch(fork()) {
			case CHILD:
				sa_int.sa_handler = &child_handler;
				sigaction(SIGINT, &sa_int, NULL);
				char sig_byte;

				/* On recupere les infos du client */
				Getnameinfo((SA*)&clientaddr, clientlen, 
						client_hostname, MAX_NAME_LEN, 0, 0, 0);

				/* On recupere une representation textuelle de l'ip client */
				Inet_ntop(AF_INET, &clientaddr.sin_addr, client_ip_string,
						INET_ADDRSTRLEN);

				printf(ANSI_COLOR_CYAN "[CONNECTION]" ANSI_COLOR_RESET);
				printf("Connexion accepted from %s (%s)\n", client_hostname,
						client_ip_string);
				rio_t rio;
				char buf[MAX_NAME_LEN];
				Rio_readinitb(&rio, clientfd);

				/* On recupere le mot de passe */
				Rio_readnb(&rio, buf, MAX_NAME_LEN);
				if(!strcmp(buf, "motdepasse")) {
					printf(ANSI_COLOR_GREEN "[OK]" ANSI_COLOR_RESET);
					printf("Client authenticated.\n");
					sig_byte = OK;
					Rio_writen(clientfd, &sig_byte, 1);
				} else {
					printf(ANSI_COLOR_RED "[ERROR]" ANSI_COLOR_RESET);
					printf("Client submited a wrong password.\n");
					sig_byte = ERROR;
					Rio_writen(clientfd, &sig_byte, 1);
					Close(clientfd);
					exit(0);
				}

				/* On attend les requetes du client et on les connecte a un 
				 * esclave pour la remplir */
				master_behaviour(clientfd, client_hostname, slaves_IPs, pwd);

				/* On ferme le socket de connexion */
				Close(clientfd);
				exit(0);
				break;
			case FORK_ERR:
				break;
			default: // Pere
				break;
			}
		}
		exit(0);
	}
}
