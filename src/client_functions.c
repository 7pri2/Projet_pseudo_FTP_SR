#include <stdio.h>
#include "client_functions.h"
#include "generic_functions.h"
#include "constants.h"
#include "csapp.h"
#include "sys/time.h"

void request_file(int masterfd, char* filename) {
	int n, nb_bytes = 0, filefd;
	off_t r_bytes = 0;
	char buf[BLOC_SIZE], sig_byte;
	clock_t t1, t2;
	rio_t rio;

	/* On envoie le code de l'action que nous allons réaliser */
	sig_byte = GET;
	Rio_writen(masterfd, &sig_byte, 1);

	/* On récupère le socket de l'esclave */
	int slavefd = get_slavefd();
	Rio_readinitb(&rio, slavefd);

	/* On envoie le nom du fichier */
	Rio_writen(slavefd, filename, strlen(filename));

	/* On attend l'octet de "signalisation" */
	rio_readnb(&rio, &sig_byte, 1);

	/* Mesure du temps pour calculer la duree du transfert */
	t1 = clock();

	if(sig_byte == OK) {
		/* On retire le retour charriot */
		remove_cr(filename);

		/* On regarde si on a un morceau de fichier déjà en local */
		if((filefd = open(filename, O_RDONLY)) != -1) {
			r_bytes = lseek(filefd, 0, SEEK_END);
			Close(filefd);
			/* On recoit la date de derniere modif du fichier distant pour la 
			 * comparer avec le fichier local */
			struct stat filestats;
			stat(filename, &filestats);
			rio_readnb(&rio, buf, BLOC_SIZE);
			time_t remote_date = atoi(buf), local_date = filestats.st_mtim.tv_sec;
			if(remote_date <= local_date) {
				printf("%d bytes of file %s already downloaded. ", 
						(int)r_bytes, filename);
				printf("Attempting to download the remaining bytes.\n");
			} else {
				printf("File partially downloaded (%d bytes already)", (int)r_bytes);
				printf(", but the remote file is newer. "); 
				printf("Downloading the whole file.\n");
				r_bytes = 0;
			}

			r_bytes = remote_date < local_date ? r_bytes : 0;
		} else {
			/* On fait une lecture inutile pour récupérer la date de dernière modif
			 * fu fichier distant, car le serveur l'envoie */
			rio_readnb(&rio, buf, BLOC_SIZE);
		}

		/* On envoie le nombre de bytes deja lus */
		sprintf(buf, "%d", (int)r_bytes);
		Rio_writen(slavefd, buf, BLOC_SIZE);
		/* On récupère les blocs que constituent la réponse du serveur */
		filefd = Open(filename, O_CREAT | O_WRONLY, 0644);
		if((int)r_bytes != 0)
			lseek(filefd, r_bytes, SEEK_SET);
		while((n = rio_readnb(&rio, buf, BLOC_SIZE)) != 0){
			Rio_writen(filefd, buf, n);
			nb_bytes += n;
		}
		Close(filefd);
		/* Calcul du delta de temps pour la duree du traitement */
		t2 = clock();
		float diff = ((float)(t2 - t1) / 1000000.0F ) * 1000; 
		printf("%d bytes received in %.2f seconds (%.2f KB/s)\n", nb_bytes, 
				diff/1000, (float)(nb_bytes/1000)/(diff/1000));
	} else {
		printf("File doesn't exist on the server.\n");
	}
	Close(slavefd);
}

int get_slavefd() {
	int listenfd = Open_listenfd(DATA_PORT);
	int slavefd = Accept(listenfd, NULL, NULL);
	Close(listenfd);
	return slavefd;
}

char** str_split(char* base_str, int parts) {
	char** str_table = (char**)malloc(sizeof(char*)*(parts+1));
	str_table[0] = base_str;

	char* buff = strtok(base_str, " ");
	int i;

	str_table[1] = buff;
	for(i = 1; i < parts && buff != NULL; ++i) {
		buff = strtok(NULL, " ");
		str_table[i] = buff;
	}
	str_table[parts+1] = NULL;
	return str_table;
}

void request_ls(int clientfd) {
	int n, nb_bytes = 0;
	rio_t rio;
	char buf_list[BLOC_SIZE], sig_byte = LS;

	/* On envoie l'octet de signalisation */
	Rio_writen(clientfd, &sig_byte, 1);

	/* On récupère le socket de l'esclave */
	int slavefd = get_slavefd();
	Rio_readinitb(&rio, slavefd);

	/* On attend l'octet de signalisation */
	rio_readnb(&rio, &sig_byte, 1);
	if(sig_byte == OK) {
		/* On récupère les blocs que constituent la réponse du serveur */
		while((n = rio_readnb(&rio, buf_list, BLOC_SIZE)) != 0){
			Rio_writen(STDOUT_FILENO, buf_list, n);
			nb_bytes += n;
		}
	} else {
		fprintf(stderr,"Error while listing remote directory.\n");
	}
}

void request_pwd(int clientfd) {
	rio_t rio;
	char buf_pwd[MAX_NAME_LEN], sig_byte = PWD;
	
	Rio_readinitb(&rio, clientfd);

	/* On envoie l'octet de signalisation */
	Rio_writen(clientfd, &sig_byte, 1);

	/* On attend l'octet de signalisation */
	rio_readnb(&rio, &sig_byte, 1);
	if(sig_byte == OK) {
		rio_readnb(&rio, buf_pwd, MAX_NAME_LEN);
		printf("%s\n", buf_pwd);
	} else {
		fprintf(stderr,"Can't get remote working directory.\n");
	}
}

void request_cd(int clientfd, char* newdir) {
	rio_t rio;
	char sig_byte = CD;

	Rio_readinitb(&rio, clientfd);

	/* On envoie l'octet de signalisation */
	Rio_writen(clientfd, &sig_byte, 1);

	/* On envoie le nom du repertoire */
	Rio_writen(clientfd, newdir, MAX_NAME_LEN);

	/* On attend l'octet de signalisation */
	rio_readnb(&rio, &sig_byte, 1);
	if(sig_byte == ERROR) {
		fprintf(stderr,"Can't change working directory.\n");
	}
}
