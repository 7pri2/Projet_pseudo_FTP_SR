#include "generic_functions.h"
#include <stdio.h>

void remove_cr(char* string) {
	int i;
	for(i = 0; string[i]; ++i)
		if(string[i] == '\n')
			string[i] = '\0';
}

void remove_last_dir(char* string) {
	int i;
	/* On part a la recherche de la fin de la chaine */
	for(i = 0; string[i]; ++i);
	/* On supprime jusqu'au prochain `/' */
	for(i = i; string[i] != '/'; --i)
		string[i] = 0;
	if(i != 0) /* Si c'est autre chose que la racine */
		string[i] = 0; /* On efface le dernier slash */
}
