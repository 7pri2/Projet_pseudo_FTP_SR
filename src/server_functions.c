#include "server_functions.h"
#include "constants.h"
#include "csapp.h"
#include "generic_functions.h"

void slave_behaviour(int connectfd, char sig_byte, char* pwd) {
	/* Recoit le byte contenant l'information de la commande a executer, cette 
	 * fonction appelle ensuite les autres fonctions */
	rio_t rio_client;

	Rio_readinitb(&rio_client, connectfd);
	switch(sig_byte) {
	case GET:
		switch(transfer(rio_client, connectfd, pwd)) {
		case 1: fprintf(stderr, "Filename not received.\n");	break;
		case 2: fprintf(stderr, "Can't send ERROR code.\n");	break;
		case 3: fprintf(stderr, "Can't sen OK code.\n");	break;
		case 4: fprintf(stderr, "Client disconnected.\n");	break;
		default:	break;
		}
		break;
	case LS:
		printf("Command `ls' requested.\n");
		ls(connectfd, pwd);
		break;
	case PUT:
		break;
	case NO_CODE:
		fprintf(stderr, "Client disconnected.\n");
		break;
	default:
		fprintf(stderr,"Unrecognized command.\n");
	}
}

void master_behaviour(int clientfd, char* client_hostname, iplist slaveIPs, char* pwd) {
	setvbuf(stdout, NULL, _IONBF, 0);
	int slavefd, jobs_done = 0, i;
	char sig_byte = NO_CODE;
	char buffer[MAX_NAME_LEN];
	rio_t rio_client;

	Rio_readinitb(&rio_client, clientfd);
	
	printf("Waiting for request.\n");
	/* On attend des requetes */
	while(Rio_readnb(&rio_client, &sig_byte, 1) > 0) {
		if(sig_byte == PWD) {
		/* Si on demande le repertoire courant, on l'a deja */
			printf(ANSI_COLOR_YELLOW "[REQUEST]" ANSI_COLOR_RESET);
			printf("Command `pwd' requested\n");
			sig_byte = OK;
			printf(ANSI_COLOR_GREEN "[OK]" ANSI_COLOR_RESET);
			printf("Working directory send to client\n");
			Rio_writen(clientfd, &sig_byte, 1);
			Rio_writen(clientfd, pwd, MAX_NAME_LEN);
		} else if(sig_byte == CD) {
		/* Si on demande de changer le repertoire courant, on l'a deja aussi */
			printf(ANSI_COLOR_YELLOW "[REQUEST]" ANSI_COLOR_RESET);
			printf("Command `cd' requested\n");
			Rio_readnb(&rio_client, buffer, MAX_NAME_LEN);
			remove_cr(buffer);
			/* On tente de changer de repertoire courant */
			char tmppwd[MAX_NAME_LEN];
			if(buffer[0] == '/') {
			/* Si c'est un chemin absolu */
				sprintf(tmppwd, "%s", buffer);
			} else if(!strcmp(buffer, "..")) {
			/* Si c'est le repertoire d'avant, on supprime le dernier 
			 * repertoire */
				sprintf(tmppwd, "%s", pwd);
				remove_last_dir(tmppwd);
			} else if(!strcmp(buffer, ".")) {
			/* C'est le repertoire courant, on ne fait rien */
				;
			} else {
			/* Sinon c'est un chemin relatif */
				sprintf(tmppwd, "%s/%s", pwd, buffer);
			}
			if(chdir(tmppwd) == -1) {
				sig_byte = ERROR;
				printf(ANSI_COLOR_RED "[ERROR]" ANSI_COLOR_RESET);
				printf("Can't change remote directory\n");
			} else {
				sig_byte = OK;
				printf(ANSI_COLOR_GREEN "[OK]" ANSI_COLOR_RESET);
				printf("Remote directory changed\n");
				pwd = tmppwd;
			}
			Rio_writen(clientfd, &sig_byte, 1);
		} else {
			/* On regarde si un esclave est disponible */
			printf("Searching for available slave\n");
			do {
				for(i = 0; i < NB_SLAVES; ++i) {
					printf(ANSI_COLOR_MAGENTA "[FORWARDING]" ANSI_COLOR_RESET);
					printf("Trying with slave number %d: ", i);
					if((slavefd = Open_clientfd(slaveIPs[i], SLAVE_PORT)) != -1) {
						printf(ANSI_COLOR_GREEN "Available !\n" ANSI_COLOR_RESET);
						jobs_done = 1;
						printf(ANSI_COLOR_MAGENTA "[FORWARDING]" ANSI_COLOR_RESET);
						printf("Forwarding request code to slave\n");
						/* On envoie le code de requete */
						Rio_writen(slavefd, &sig_byte, 1);
						
						/* On envoie les infos du client a l'esclave */
						printf(ANSI_COLOR_MAGENTA "[FORWARDING]" ANSI_COLOR_RESET);
						printf("Forwarding client hostname to slave\n");
						Rio_writen(slavefd, client_hostname , MAX_NAME_LEN);

						/* On envoie le pwd a l'esclave */
						printf(ANSI_COLOR_MAGENTA "[FORWARDING]" ANSI_COLOR_RESET);
						printf("Forwarding working directory to slave\n");
						Rio_writen(slavefd, pwd, MAX_NAME_LEN);
						Close(slavefd);
						break;
					} else {
						printf(ANSI_COLOR_RED "Busy\n" ANSI_COLOR_RESET);
					}
				}
				sleep(1); /* On attend un petit temps avant de recommencer */
			} while(!jobs_done);
		}
	}
	printf(ANSI_COLOR_CYAN "[CONNECTION]" ANSI_COLOR_RESET);
	printf("Client disconnected.\n");
}

int transfer(rio_t rio, int connectfd, char* pwd) {
	UNUSED(pwd);
	size_t n;
	int filefd, bytes_read;
	char *filename = (char*)malloc(sizeof(char)*MAX_NAME_LEN), buffer[BLOC_SIZE], sig_byte;
	rio_t rio_fichier;

	/* On récupère du client le nom du fichier */
	if(rio_readlineb(&rio, buffer, MAX_NAME_LEN) == -1)
		return 1;

	/* On retire le retour charriot */
	remove_cr(buffer);
	
	sprintf(filename, "%s/%s", pwd, buffer);

	printf(ANSI_COLOR_YELLOW "[REQUEST]" ANSI_COLOR_RESET);
	printf("File `%s' requested for transfer.\n", filename);

	/* On ouvre le fichier en lecture pour transmettre son contenu */
	if((filefd = open(filename, O_RDONLY, 0)) == -1) {
		printf(ANSI_COLOR_RED "[ERROR]" ANSI_COLOR_RESET);
		fprintf(stderr, "file `%s' doesn't exist.\n", filename);
		/* On envoie un octet pour signaler l'erreur */
		sig_byte = ERROR;
		
		if(rio_writen(connectfd, &sig_byte, 1) == -1)
			return 2;
	} else {
		Rio_readinitb(&rio_fichier, filefd);
		/* On envoie un octet pour signaler que le fichier existe et que le 
		 * contenu va etre envoye au client */
		sig_byte = OK;
		if(rio_writen(connectfd, &sig_byte, 1) == -1)
			return 3;
		/* On envoie la date de derniere modification du fichier */
		struct stat filestats;
		stat(filename, &filestats);
		sprintf(buffer, "%lld", (long long)filestats.st_mtim.tv_sec);
		Rio_writen(connectfd, buffer, BLOC_SIZE);

		/* On recupere le nombre de bytes déjà telechargés */
		Rio_readnb(&rio, &buffer, BLOC_SIZE);
		bytes_read = atoi(buffer);

		lseek(rio_fichier.rio_fd, bytes_read, SEEK_SET);
		while((n = Rio_readnb(&rio_fichier, buffer, BLOC_SIZE)) != 0) {
			if(rio_writen(connectfd, buffer, n) == -1)
				return 4;
		}
		printf(ANSI_COLOR_GREEN "[OK]" ANSI_COLOR_RESET);
		printf("File `%s' transferred successfully.\n", filename);
	}
	return 0;
}

int ls(int connectfd, char* pwd) {
	pid_t pid;
	char sig_byte, *args[] = {"ls", pwd, NULL};

	if((pid = fork()) == 0) {
		/* Child */
		/* On envoie le code au client qui lui annonce que tout s'est bien passé */
		sig_byte = OK;
		printf(ANSI_COLOR_GREEN "[OK]" ANSI_COLOR_RESET);
		printf("Sending file list to client\n");
		Rio_writen(connectfd, &sig_byte, 1);

		/* On redirige la sortie de ls sur le socket */
		dup2(connectfd, STDOUT_FILENO);
		execv("/bin/ls", args);
		exit(0);
	} else if(pid != -1){
		/* Parent */
		waitpid(pid, NULL, 0);
	} else {
		fprintf(stderr, "Fork error.\n");
		/* On envoie le code au client qui lui annonce une erreur */
		sig_byte = ERROR;
		Rio_writen(connectfd, &sig_byte, 1);
	}
	return 0;
}

