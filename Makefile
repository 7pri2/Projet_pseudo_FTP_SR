INCLUDE=include/
OBJ=obj/
SRC=src/

CC=gcc
CFLAGS=-Wall -Werror -Wextra -I$(INCLUDE)
LDFLAGS=-lpthread

EXECUTABLES=client server

OBJFILES=client.o			\
		 server.o 			\
		 server_functions.o \
		 generic_functions.o\
		 client_functions.o

all: $(EXECUTABLES)

debug: CFLAGS+=-g
debug: all

client: $(OBJ)client.o $(OBJ)csapp.o $(OBJ)generic_functions.o $(OBJ)client_functions.o
	$(CC) -o $@ $^ $(LDFLAGS)

server: $(OBJ)server.o $(OBJ)csapp.o $(OBJ)server_functions.o $(OBJ)generic_functions.o
	$(CC) -o $@ $^ $(LDFLAGS)

$(OBJ)%.o: $(SRC)%.c
	$(CC) $(CFLAGS) -c -o $@ $<

$(OBJ)server_functions.o: $(INCLUDE)server_functions.h $(INCLUDE)constants.h $(INCLUDE)generic_functions.h
$(OBJ)client_functions.o: $(INCLUDE)client_functions.h $(INCLUDE)constants.h $(INCLUDE)generic_functions.h
$(OBJ)server.o: $(INCLUDE)constants.h $(INCLUDE)server_functions.h
$(OBJ)client.o: $(INCLUDE)constants.h $(INCLUDE)client_functions.h $(INCLUDE)generic_functions.h
$(OBJ)generic_functions.o: $(INCLUDE)generic_functions.h

test: $(EXECUTABLES)
	bash tests/autotest.sh reset

test_no_reset: $(EXECUTABLES)
	bash tests/autotest.sh

clean: 
	rm -rf $(patsubst %.o, $(OBJ)%.o, $(OBJFILES)) $(EXECUTABLES)
